export const environment = {
  production: true,
  server: 'https://ubmart.online/api/',
  firebase: {
    apiKey: "AIzaSyBblmMJH7aqh0BI4lPvD4MndE_qitDqWtA",
    authDomain: "ubmart-online.firebaseapp.com",
    databaseURL: "https://ubmart-online.firebaseio.com",
    projectId: "ubmart-online",
    storageBucket: "ubmart-online.appspot.com",
    messagingSenderId: "967183193873",
    appId: "1:967183193873:web:dcfcacfe3c761927889eb0",
    measurementId: "G-5KRWYBRM6Q"
  }
};
