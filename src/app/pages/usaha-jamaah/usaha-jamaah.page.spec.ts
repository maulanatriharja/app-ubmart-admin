import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsahaJamaahPage } from './usaha-jamaah.page';

describe('UsahaJamaahPage', () => {
  let component: UsahaJamaahPage;
  let fixture: ComponentFixture<UsahaJamaahPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsahaJamaahPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UsahaJamaahPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
