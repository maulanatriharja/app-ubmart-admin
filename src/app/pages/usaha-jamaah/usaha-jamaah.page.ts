import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { EventsService } from '../../services/events.service';
import { Storage } from '@ionic/storage';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-usaha-jamaah',
  templateUrl: './usaha-jamaah.page.html',
  styleUrls: ['./usaha-jamaah.page.scss'],
})
export class UsahaJamaahPage implements OnInit {

  loading = true;

  data_aktif: any = [];
  data_pending: any = [];
  data_expired: any = [];

  status: string = '1'

  constructor(
    public alertController: AlertController,
    public events: EventsService,
    public http: HttpClient,
    public router: Router,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.events.obvUsaha().subscribe(() => {
      this.load_all();
    });

    this.load_all();
  }

  segmentChanged() {
    this.load_all();
  }

  load_all() {
    this.load_data_aktif();
    this.load_data_pending();
    this.load_data_expired();
  }

  load_data_aktif() {
    let url_cek = environment.server + 'usaha_jamaah/usahajamaah_check_expired.php';
    this.http.get(url_cek).subscribe(() => {

      let url = environment.server + 'usaha_jamaah/usahajamaah_read_aktif.php';
      this.http.get(url).subscribe((data) => {
        console.info(data);
        this.data_aktif = data;
        this.loading = false;
      }, error => {
        console.info(error.error);
        console.info('reload data');
        this.load_data_aktif();
        this.loading = false;
      });

    }, error_cek => {
      console.info(error_cek.error);
      console.info('reload data');
    });
  }

  load_data_pending() {
    let url = environment.server + 'usaha_jamaah/usahajamaah_read.php?status=0';
    this.http.get(url).subscribe((data) => {
      console.info(data);
      this.data_pending = data;
      this.loading = false;
    }, error => {
      console.info(error.error);
      console.info('reload data');
      this.load_data_pending();
      this.loading = false;
    });
  }

  load_data_expired() {
    let url = environment.server + 'usaha_jamaah/usahajamaah_read.php?status=3';
    this.http.get(url).subscribe((data) => {
      console.info(data);
      this.data_expired = data;
      this.loading = false;
    }, error => {
      console.info(error.error);
      console.info('reload data');
      this.load_data_expired();
      this.loading = false;
    });
  }

  detail(val) {
    this.router.navigate(['usaha-jamaah-detail'], { queryParams: val });
  }

}
