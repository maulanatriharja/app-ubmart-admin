import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsahaJamaahPageRoutingModule } from './usaha-jamaah-routing.module';

import { UsahaJamaahPage } from './usaha-jamaah.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UsahaJamaahPageRoutingModule
  ],
  declarations: [UsahaJamaahPage]
})
export class UsahaJamaahPageModule {}
