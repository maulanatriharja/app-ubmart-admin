import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsahaJamaahDetailPageRoutingModule } from './usaha-jamaah-detail-routing.module';

import { UsahaJamaahDetailPage } from './usaha-jamaah-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UsahaJamaahDetailPageRoutingModule
  ],
  declarations: [UsahaJamaahDetailPage]
})
export class UsahaJamaahDetailPageModule {}
