import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { EventsService } from '../../../services/events.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-usaha-jamaah-detail',
  templateUrl: './usaha-jamaah-detail.page.html',
  styleUrls: ['./usaha-jamaah-detail.page.scss'],
})
export class UsahaJamaahDetailPage implements OnInit {

  id_usahajamaah: string;
  nama: string;
  keterangan: string;
  alamat: string;
  pemilik: string;
  no_hp: string;
  tanggal_gabung: string;
  tanggal_mulai: string;
  tanggal_berakhir: string;
  status: string;

  constructor(
    public events: EventsService,
    public http: HttpClient,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    public toastController: ToastController,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(res => {
      this.id_usahajamaah = res.id;
      this.nama = res.nama_usaha;
      this.keterangan = res.keterangan;
      this.alamat = res.alamat;
      this.pemilik = res.nama_pemilik;
      this.no_hp = res.no_hp;
      this.tanggal_gabung = res.tanggal_gabung;
      this.tanggal_mulai = res.tanggal_mulai;
      this.tanggal_berakhir = res.tanggal_berakhir;
      this.status = res.status;
    });
  }

  async update_aktif() {

    const loading = await this.loadingController.create({});
    loading.present();

    let url = environment.server + 'usaha_jamaah/usahajamaah_update_aktif.php';
    let body = new FormData();
    // body.append('id_ub', '1'); // UBAH INI
    body.append('id_usahajamaah', this.id_usahajamaah.toString());

    this.http.post(url, body).subscribe(async (data) => {
      console.info(data);

      this.events.pubAkun({});
      loading.dismiss();

      const toast = await this.toastController.create({
        message: 'Usaha Jamaah telah diaktifkan.',
        position: 'top',
        color: 'success',
        duration: 2000,
        buttons: [
          {
            text: 'X',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      toast.present();

      this.navCtrl.back();

    }, error => {
      console.error(error.error);
      loading.dismiss();
    });
  }

}
