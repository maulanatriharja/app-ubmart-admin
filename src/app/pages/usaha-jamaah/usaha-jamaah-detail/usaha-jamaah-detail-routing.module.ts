import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsahaJamaahDetailPage } from './usaha-jamaah-detail.page';

const routes: Routes = [
  {
    path: '',
    component: UsahaJamaahDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsahaJamaahDetailPageRoutingModule {}
