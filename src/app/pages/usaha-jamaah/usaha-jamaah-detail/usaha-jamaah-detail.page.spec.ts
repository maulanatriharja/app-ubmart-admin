import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsahaJamaahDetailPage } from './usaha-jamaah-detail.page';

describe('UsahaJamaahDetailPage', () => {
  let component: UsahaJamaahDetailPage;
  let fixture: ComponentFixture<UsahaJamaahDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsahaJamaahDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UsahaJamaahDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
