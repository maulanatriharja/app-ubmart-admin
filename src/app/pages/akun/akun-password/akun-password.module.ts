import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AkunPasswordPageRoutingModule } from './akun-password-routing.module';

import { AkunPasswordPage } from './akun-password.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AkunPasswordPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AkunPasswordPage]
})
export class AkunPasswordPageModule { }
