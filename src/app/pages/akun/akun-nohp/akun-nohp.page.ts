import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { EventsService } from '../../../services/events.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-akun-nohp',
  templateUrl: './akun-nohp.page.html',
  styleUrls: ['./akun-nohp.page.scss'],
})
export class AkunNohpPage implements OnInit {

  id: number;
  no_hp: string;

  proses_simpan: boolean = false;

  constructor(
    public events: EventsService,
    public navCtrl: NavController,
    public http: HttpClient,
    public route: ActivatedRoute,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.route.queryParams.subscribe(res => {
      this.id = res.id;
      this.no_hp = '0' + res.no_hp;
    })
  }

  simpan() {
    if (!this.no_hp) {
      alert('Silahkan isi nomor HP anda.')
    } else {
      this.proses_simpan = true;

      let url = environment.server + 'user_ub/user_ub_update_nohp.php';
      let body = new FormData();
      body.append('id', this.id.toString());
      body.append('no_hp', this.no_hp);

      this.http.post(url, body).subscribe((data: any) => {
        if (data.status == true) {
          this.storage.get('profil').then((val) => {
            val.no_hp = parseInt(this.no_hp);

            this.storage.set('profil', val).then(data => {
              this.events.pubAkun({});
              this.navCtrl.back();

              this.proses_simpan = false;
            });
          });
        } else {
          alert(data.message)
        }

        this.proses_simpan = false;
      }, error => {
        this.proses_simpan = false;
        console.info(error.error);
      });
    }
  }

}