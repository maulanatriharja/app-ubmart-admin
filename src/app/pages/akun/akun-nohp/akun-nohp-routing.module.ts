import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AkunNohpPage } from './akun-nohp.page';

const routes: Routes = [
  {
    path: '',
    component: AkunNohpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AkunNohpPageRoutingModule {}
