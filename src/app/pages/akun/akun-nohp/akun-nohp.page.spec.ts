import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AkunNohpPage } from './akun-nohp.page';

describe('AkunNohpPage', () => {
  let component: AkunNohpPage;
  let fixture: ComponentFixture<AkunNohpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AkunNohpPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AkunNohpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
