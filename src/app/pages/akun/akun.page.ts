import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { EventsService } from '../../services/events.service';

@Component({
  selector: 'app-akun',
  templateUrl: './akun.page.html',
  styleUrls: ['./akun.page.scss'],
})
export class AkunPage implements OnInit {

  id: number;
  nama: string;
  no_hp: string;
  password: string;

  constructor(
    public events: EventsService,
    public navCtrl: NavController,
    public router: Router,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getProfil();

    this.events.obvAkun().subscribe((data) => {
      this.getProfil();
    })
  }

  getProfil() {
    this.storage.get('profil').then((val) => {
      this.id = val.id;
      this.nama = val.nama_ub_admin;
      this.no_hp = val.no_hp;
      this.password = val.password;
    });
  }

  ubah_nama() {
    this.router.navigate(['akun-nama'],
      {
        queryParams: {
          id: this.id,
          nama: this.nama
        }
      }
    );
  }

  ubah_nohp() {
    this.router.navigate(['akun-nohp'],
      {
        queryParams: {
          id: this.id,
          no_hp: this.no_hp
        }
      }
    );
  }

  ubah_password() {
    this.router.navigate(['akun-password'],
      {
        queryParams: {
          id: this.id,
          password: this.password
        }
      }
    );
  }

  keluar() {
    this.storage.clear();
    this.navCtrl.navigateRoot('/login');
  }
}