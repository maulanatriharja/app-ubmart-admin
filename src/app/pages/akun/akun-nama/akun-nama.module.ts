import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AkunNamaPageRoutingModule } from './akun-nama-routing.module';

import { AkunNamaPage } from './akun-nama.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AkunNamaPageRoutingModule
  ],
  declarations: [AkunNamaPage]
})
export class AkunNamaPageModule {}
