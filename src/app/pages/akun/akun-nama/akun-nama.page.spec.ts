import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AkunNamaPage } from './akun-nama.page';

describe('AkunNamaPage', () => {
  let component: AkunNamaPage;
  let fixture: ComponentFixture<AkunNamaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AkunNamaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AkunNamaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
