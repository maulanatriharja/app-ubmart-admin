import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KategoriDetailPage } from './kategori-detail.page';

describe('KategoriDetailPage', () => {
  let component: KategoriDetailPage;
  let fixture: ComponentFixture<KategoriDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KategoriDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KategoriDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
