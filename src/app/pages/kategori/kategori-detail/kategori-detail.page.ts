import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../../../services/events.service';
import { Storage } from '@ionic/storage';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-kategori-detail',
  templateUrl: './kategori-detail.page.html',
  styleUrls: ['./kategori-detail.page.scss'],
})
export class KategoriDetailPage implements OnInit {

  id_kategori: string;
  nama_kategori: string;

  constructor(
    public alertController: AlertController,
    public events: EventsService,
    public http: HttpClient,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    public router: Router,
    public storage: Storage,
    public toastController: ToastController,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(res => {
      this.id_kategori = res.id_kategori;
      this.nama_kategori = res.nama_kategori;
    })
  }

  tambah() {
    if (!this.nama_kategori) {
      alert('Silahkan isi nama kategori');
    } else {
      this.storage.get('profil').then((val) => {
        let url = environment.server + 'produk/kategori_create.php';
        let body = new FormData();
        body.append('id_ub', val.ub.id_ub);
        body.append('nama_kategori', this.nama_kategori);

        this.http.post(url, body).subscribe(async (data) => {
          console.info(data);

          this.events.pubKategori({});
          this.navCtrl.navigateRoot('/tabs/kategori');

          const toast = await this.toastController.create({
            message: 'Berhasil menambah kategori baru.',
            position: 'top',
            color: 'success',
            duration: 2000,
            buttons: [
              {
                text: 'X',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              }
            ]
          });
          toast.present();
        }, error => {
          console.info(error.error);
        });
      });
    }
  }

  update() {
    if (!this.nama_kategori) {
      alert('Silahkan isi nama kategori');
    } else {
      let url = environment.server + 'produk/kategori_update.php';
      let body = new FormData();
      // body.append('id_ub', '1'); // UBAH INI
      body.append('id', this.id_kategori);
      body.append('nama_kategori', this.nama_kategori);

      this.http.post(url, body).subscribe(async (data) => {
        console.info(data);

        this.events.pubKategori({});
        this.navCtrl.navigateRoot('/tabs/kategori');

        const toast = await this.toastController.create({
          message: 'Berhasil mengubah kategori.',
          position: 'top',
          color: 'success',
          duration: 2000,
          buttons: [
            {
              text: 'X',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        });
        toast.present();
      }, error => {
        console.info(error.error);
      });
    }
  }

  async hapus() {
    const alert = await this.alertController.create({
      cssClass: 'alert-class',
      subHeader: 'Konfirmasi',
      message: 'Yakin untuk menghapus kategori "' + this.nama_kategori + '" ?',
      mode: 'ios',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Hapus',
          handler: async () => {
            let url = environment.server + 'produk/kategori_delete.php';
            let body = new FormData();
            body.append('id', this.id_kategori);

            this.http.post(url, body).subscribe(async (data) => {
              console.info(data);

              this.events.pubKategori({});
              this.navCtrl.navigateRoot('/tabs/kategori');

              const toast = await this.toastController.create({
                message: 'Kategori telah dihapus.',
                position: 'top',
                color: 'light',
                duration: 2000,
                buttons: [
                  {
                    text: 'X',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  }
                ]
              });
              toast.present();
            }, error => {
              console.info(error.error);
            });
          }

        }
      ]
    });
    await alert.present();
  }
}
