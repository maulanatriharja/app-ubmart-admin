import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KategoriDetailPageRoutingModule } from './kategori-detail-routing.module';

import { KategoriDetailPage } from './kategori-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KategoriDetailPageRoutingModule
  ],
  declarations: [KategoriDetailPage]
})
export class KategoriDetailPageModule {}
