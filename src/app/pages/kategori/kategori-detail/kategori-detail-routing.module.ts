import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KategoriDetailPage } from './kategori-detail.page';

const routes: Routes = [
  {
    path: '',
    component: KategoriDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KategoriDetailPageRoutingModule {}
