import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { EventsService } from '../../services/events.service';
import { Storage } from '@ionic/storage';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-kategori',
  templateUrl: './kategori.page.html',
  styleUrls: ['./kategori.page.scss'],
})
export class KategoriPage implements OnInit {

  data: any = [];
  loading = true;

  constructor(
    public alertController: AlertController,
    public events: EventsService,
    public http: HttpClient,
    public router: Router,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.events.obvKategori().subscribe(() => {
      this.load_data();
    });

    this.load_data();
  }

  load_data() {
    this.storage.get('profil').then((val) => {
      let url = environment.server + 'produk/kategori_read.php?id_ub=' + val.ub.id_ub;

      this.http.get(url).subscribe((data) => {
        console.info(data);

        this.data = data;

        this.loading = false;
      }, error => {
        console.info(error.error);
        console.info('reload data');
        this.load_data();
        this.loading = false;
      });
    });
  }

  tambah() {
    this.router.navigate(['kategori-detail'],
      {
        queryParams: {
          // pesanan: JSON.stringify(this.param)
        }
      }
    );
  }

  update(val_id, val_nama_kategori) {
    this.router.navigate(['kategori-detail'],
      {
        queryParams: {
          id_kategori: val_id,
          nama_kategori: val_nama_kategori
        }
      }
    );
  }

}
