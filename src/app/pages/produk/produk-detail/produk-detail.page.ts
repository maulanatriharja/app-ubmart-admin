import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActionSheetController, AlertController, NavController, LoadingController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../../../services/events.service';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder } from "@angular/forms";
import { environment } from '../../../../environments/environment';

import * as moment from 'moment';
import { parse } from 'querystring';

@Component({
  selector: 'app-produk-detail',
  templateUrl: './produk-detail.page.html',
  styleUrls: ['./produk-detail.page.scss'],
})
export class ProdukDetailPage implements OnInit {

  myForm: FormGroup;
  isSubmitted = false;
  chip_err: string;

  ub_id: number;

  data_kategori: any = [];

  id_produk: string = '';

  foto_path: any;
  foto: string;
  foto_old: string;
  win: any = window;

  load: HTMLIonLoadingElement = null;
  persen: any;

  toggle_stok: string = 'false';

  constructor(
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public camera: Camera,
    public events: EventsService,
    public filePath: FilePath,
    public fileTransfer: FileTransfer,
    public formBuilder: FormBuilder,
    public http: HttpClient,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    public router: Router,
    public storage: Storage,
    public toastController: ToastController,
  ) {

  }

  format(val) {
    return val.replace(/\./g, '').replace(/\B(?=(?:\d{3})+(?!\d))/g, '.');
  };

  ngOnInit() {

    this.route.queryParams.subscribe(res => {

      this.data_kategori = JSON.parse(res.data_kategori);

      this.id_produk = res.id_produk;

      this.foto = environment.server + 'produk/foto/' + res.foto + '.png';
      this.foto_old = res.foto;

      if (res.stok_tersedia == 1) {
        this.toggle_stok = 'true'
      } else {
        this.toggle_stok = 'false'
      }

      if (res.id_produk) {
        this.myForm = this.formBuilder.group({
          id_kategori: [res.id_kategori],
          nama_produk: [res.nama_produk],
          harga_pokok: [this.setRibuan(res.harga_pokok)],
          harga_jual: [this.setRibuan(res.harga_jual)],
          keterangan: [res.keterangan],
        });
      } else {
        this.myForm = this.formBuilder.group({
          id_kategori: [''],
          nama_produk: [''],
          harga_pokok: [''],
          harga_jual: [''],
          keterangan: [''],
        });
      }

    });



    this.storage.get('profil').then((val) => {
      this.ub_id = val.ub.id_ub;
    });
  }

  async ubah_foto() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Update Photo',
      buttons: [{
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          console.info('Open Camera');
          this.openCamera();
        }
      }, {
        text: 'Gallery',
        icon: 'images',
        handler: () => {
          console.info('Open Gallery');
          this.openGallery();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  openCamera() {
    let options = {
      allowEdit: true,
      correctOrientation: true,
      encodingType: this.camera.EncodingType.PNG,
      quality: 90,
      sourceType: this.camera.PictureSourceType.CAMERA,
      targetHeight: 400,
      targetWidth: 400,
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.filePath.resolveNativePath(imagePath).then(async filePath => {
        this.foto_path = filePath;
        this.foto = this.win.Ionic.WebView.convertFileSrc(filePath);;
      });
    }, (err) => {
      alert('Error while selecting image.');
      //this.alertCtrl.create({subTitle: 'ERR : ' + error + ' ' + JSON.stringify(error), buttons: ['OK']}).present();
    });
  }

  openGallery() {
    let options = {
      allowEdit: true,
      correctOrientation: true,
      encodingType: this.camera.EncodingType.PNG,
      quality: 90,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetHeight: 400,
      targetWidth: 400,
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.filePath.resolveNativePath(imagePath).then(async filePath => {

        this.foto_path = filePath;
        this.foto = this.win.Ionic.WebView.convertFileSrc(filePath) + '?time=' + moment();
      });
    }, (err) => {
      alert('Error while selecting image.');
      //this.alertCtrl.create({subTitle: 'ERR : ' + error + ' ' + JSON.stringify(error), buttons: ['OK']}).present();
    });
  }

  ubah_stok() {
    if (this.toggle_stok == 'true') {
      this.toggle_stok = 'false';
    } else {
      this.toggle_stok = 'true';
    }
  }

  setRibuan(val) {
    return val.toString().replace(/\./g, '').replace(/\B(?=(?:\d{3})+(?!\d))/g, '.')
  }

  setHargaPokok() {
    this.myForm.patchValue({ harga_pokok: [this.setRibuan(this.myForm.get('harga_pokok').value)] });
  }

  setHargaJual() {
    this.myForm.patchValue({ harga_jual: [this.setRibuan(this.myForm.get('harga_jual').value)] });
  }

  async submit() {

    this.isSubmitted = true;

    if (this.myForm.valid) {

      const loading = this.loadingController.create({
        message: this.persen,
      }).then((load: HTMLIonLoadingElement) => {
        this.load = load;
        this.load.present().then(() => {
          // if (!this.isLoading) {
          //   this.load.dismiss().then();
          // }
        });
      });

      // const loading = await this.loadingController.create({ message: this.persen });
      // await loading.present();

      let newFileName = this.ub_id + '_' + Math.random();

      if (!this.id_produk) {
        //INSERT NEW
        this.storage.get('profil').then((val) => {
          let url = environment.server + 'produk/produk_create.php';
          let body = new FormData();
          body.append('id_ub', val.ub.id_ub);
          body.append('id_kategori', this.myForm.get('id_kategori').value);
          body.append('nama_produk', this.myForm.get('nama_produk').value);
          body.append('harga_pokok', this.myForm.get('harga_pokok').value.toString().replace(/\./g, ''));
          body.append('harga_jual', this.myForm.get('harga_jual').value.toString().replace(/\./g, ''));
          body.append('keterangan', this.myForm.get('keterangan').value);
          body.append('foto', newFileName);
          body.append('stok_tersedia', this.toggle_stok);

          this.http.post(url, body).subscribe(async (data) => {
            console.info(data);

            const fileTransfer: FileTransferObject = this.fileTransfer.create();
            const backend = environment.server + 'produk/foto_upload.php';

            let options: FileUploadOptions = {
              fileKey: 'file',
              fileName: newFileName + '.png',
              headers: { Accept: 'application/json', Authorization: 'Bearer ' }
            }

            if (this.foto_path) {

              fileTransfer.onProgress(async (progressEvent) => {
                var perc = Math.floor(progressEvent.loaded / progressEvent.total * 100) + 1;
                this.persen = perc;

                this.load.message = perc + '%';
              });

              fileTransfer.upload(this.foto_path, backend, options).then(async (entry) => {
                this.events.pubProduk({});
                this.navCtrl.navigateRoot('/tabs/produk');

                this.load.dismiss();

                const toast = await this.toastController.create({
                  message: 'Berhasil menambah produk baru.',
                  position: 'top',
                  color: 'success',
                  duration: 2000,
                  buttons: [
                    {
                      text: 'X',
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    }
                  ]
                });
                toast.present();

              }, (error) => {
                alert(JSON.stringify(error));
                this.load.dismiss();
              });

            } else {
              this.events.pubProduk({});
              this.navCtrl.navigateRoot('/tabs/produk');

              this.load.dismiss();

              const toast = await this.toastController.create({
                message: 'Berhasil menambah produk baru.',
                position: 'top',
                color: 'success',
                duration: 2000,
                buttons: [
                  {
                    text: 'X',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  }
                ]
              });
              toast.present();
            }

          }, error => {
            console.info(error.error);
            console.info('reload submit()');
            this.submit();
          });
        });
      } else {
        //UPDATE

        let stok_available;

        if (this.toggle_stok == 'true') {
          stok_available = 1;
        } else {
          stok_available = 0;
        }

        let url = environment.server + 'produk/produk_update.php';
        let body = new FormData();
        body.append('id', this.id_produk);
        body.append('id_kategori', this.myForm.get('id_kategori').value);
        body.append('nama_produk', this.myForm.get('nama_produk').value);
        body.append('harga_pokok', this.myForm.get('harga_pokok').value.toString().replace(/\./g, ''));
        body.append('harga_jual', this.myForm.get('harga_jual').value.toString().replace(/\./g, ''));
        body.append('keterangan', this.myForm.get('keterangan').value);
        body.append('foto_old', this.foto_old);
        body.append('stok_tersedia', stok_available);

        if (this.foto_path) {
          body.append('foto', newFileName);
        } else {
          body.append('foto', this.foto_old);
        }

        this.http.post(url, body).subscribe(async (data) => {
          console.info(data);

          const fileTransfer: FileTransferObject = this.fileTransfer.create();
          const backend = environment.server + 'produk/foto_upload.php';

          let options: FileUploadOptions = {
            fileKey: 'file',
            fileName: newFileName + '.png',
            headers: { Accept: 'application/json', Authorization: 'Bearer ' }
          }

          if (this.foto_path) {
            fileTransfer.onProgress(async (progressEvent) => {
              var perc = Math.floor(progressEvent.loaded / progressEvent.total * 100) + 1;
              this.persen = perc;

              this.load.message = perc + '%';
            });

            fileTransfer.upload(this.foto_path, backend, options).then(async (entry) => {
              this.events.pubProduk({});
              this.navCtrl.navigateRoot('/tabs/produk');

              this.load.dismiss();

              const toast = await this.toastController.create({
                message: 'Berhasil mengubah produk.',
                position: 'top',
                color: 'success',
                duration: 2000,
                buttons: [
                  {
                    text: 'X',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  }
                ]
              });
              toast.present();

            }, (error) => {
              alert(JSON.stringify(error));
              this.load.dismiss();
            });
          } else {
            this.events.pubProduk({});
            this.navCtrl.navigateRoot('/tabs/produk');

            this.load.dismiss();

            const toast = await this.toastController.create({
              message: 'Berhasil mengubah produk.',
              position: 'top',
              color: 'success',
              duration: 2000,
              buttons: [
                {
                  text: 'X',
                  role: 'cancel',
                  handler: () => {
                    console.log('Cancel clicked');
                  }
                }
              ]
            });
            toast.present();
          }

        }, error => {
          console.info(error.error);
          console.info('reload submit()');
          this.submit();
        });
      }

    }
  }

  async hapus() {
    const alert = await this.alertController.create({
      cssClass: 'alert-class',
      subHeader: 'Konfirmasi',
      message: 'Yakin untuk menghapus produk ?',
      mode: 'ios',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Hapus',
          handler: async () => {
            let url = environment.server + 'produk/produk_delete.php';
            let body = new FormData();
            body.append('id', this.id_produk);

            this.http.post(url, body).subscribe(async (data) => {
              console.info(data);

              this.events.pubProduk({});
              this.navCtrl.navigateRoot('/tabs/produk');

              const toast = await this.toastController.create({
                message: 'Produk telah dihapus.',
                position: 'top',
                color: 'light',
                duration: 2000,
                buttons: [
                  {
                    text: 'X',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  }
                ]
              });
              toast.present();
            }, error => {
              console.info(error.error);
              console.info('reload hapus()');
              this.hapus();
            });
          }

        }
      ]
    });
    await alert.present();
  }
}
