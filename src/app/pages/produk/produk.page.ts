import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { EventsService } from '../../services/events.service';
import { Storage } from '@ionic/storage';
import { environment } from '../../../environments/environment';

import * as moment from 'moment';

@Component({
  selector: 'app-produk',
  templateUrl: './produk.page.html',
  styleUrls: ['./produk.page.scss'],
})
export class ProdukPage implements OnInit {

  cari: string;
  infi: any;

  id_kategori: string;

  data_kategori: any = [];
  data: any = [];

  loading = true;

  time: any;

  limit: number = 0;
  items_max: number = 8;

  random_sort: number = Math.floor((Math.random() * 10) + 1);

  constructor(
    public alertController: AlertController,
    public events: EventsService,
    public http: HttpClient,
    public router: Router,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.time = moment();

    this.events.obvProduk().subscribe(() => {
      this.data = [];
      this.load_data('', '', '');
    });

    this.load_data_kategori();
    this.load_data('', '', '');
  }

  load_data_kategori() {
    this.storage.get('profil').then((val) => {
      let url = environment.server + 'produk/kategori_read.php?id_ub=' + val.ub.id_ub;

      this.http.get(url).subscribe((data) => {
        console.info(data);
        this.data_kategori = data;
      }, error => {
        console.info(error.error);
      });
    });
  }

  reset_limit() {
    this.limit = 0;
    this.data = [];
  }

  load_data(event_segment, event_cari, event_infi) {
    if (!event_cari) {
      event_cari = '';
    }

    this.cari = event_cari;

    this.storage.get('profil').then((val) => {
      // this.loading = true;

      // let url = environment.server + 'produk/produk_read.php?id_ub=' + val.ub.id_ub + '&id_kategori=' + this.id_kategori + '&sort=nama_produk';
      let url = environment.server + 'produk/produk_read.php?id_ub=' + val.ub.id_ub + '&id_kategori=' + event_segment + '&q=' + event_cari
        + '&sort=RAND(' + this.random_sort + ')&limit=' + this.limit;

      this.limit += this.items_max;

      this.http.get(url).subscribe((data: any) => {
        // console.info("DATAAAA : " + JSON.stringify(data));
        // this.data = data;

        for (let i = 0; i < data.length; i++) {
          this.data.push(data[i]);
        }

        if (data.length < this.items_max) {
          event_infi.target.disabled = true;
        }

        this.loading = false;
      }, error => {
        console.info(error.error);
        console.info('reload data');
        this.load_data(event_segment, event_cari, event_infi)
        this.loading = false;
      });

      event_infi.target.complete();

    });
  }

  tambah() {
    this.router.navigate(['produk-detail'],
      {
        queryParams: {
          data_kategori: JSON.stringify(this.data_kategori),
        }
      });
  }

  update(val_id_kategori, val_id, val_nama_produk, val_harga_pokok, val_harga_jual, val_foto, val_keterangan, val_stok_tersedia, val_stok_total) {
    this.router.navigate(['produk-detail'],
      {
        queryParams: {
          data_kategori: JSON.stringify(this.data_kategori),

          id_kategori: val_id_kategori,
          id_produk: val_id,
          nama_produk: val_nama_produk,
          harga_pokok: val_harga_pokok,
          harga_jual: val_harga_jual,
          foto: val_foto,
          keterangan: val_keterangan,
          stok_tersedia: val_stok_tersedia,
          stok_total: val_stok_total,
        }
      }
    );
  }
}