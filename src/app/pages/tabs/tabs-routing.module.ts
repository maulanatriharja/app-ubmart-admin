import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'dashboard',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../dashboard/dashboard.module').then(m => m.DashboardPageModule)
          }
        ]
      },
      {
        path: 'kategori',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../kategori/kategori.module').then(m => m.KategoriPageModule)
          }
        ]
      },
      {
        path: 'produk',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../produk/produk.module').then(m => m.ProdukPageModule)
          }
        ]
      },

      {
        path: 'transaksi',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../transaksi/transaksi.module').then(m => m.TransaksiPageModule)
          }
        ]
      },
      {
        path: 'kurir',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../kurir/kurir.module').then(m => m.KurirPageModule)
          }
        ]
      },
      {
        path: 'usaha-jamaah',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../usaha-jamaah/usaha-jamaah.module').then(m => m.UsahaJamaahPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/dashboard',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
