import { Component } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  judul: string = "Beranda";

  constructor() { }

  tap(val) {
    this.judul = val;
  }

}
