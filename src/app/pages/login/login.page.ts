import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { environment } from '../../../environments/environment';

import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  myform = {
    no_hp: '',
    password: '',
  };

  proses_login: boolean = false;

  constructor(
    public firebaseX: FirebaseX,
    public http: HttpClient,
    public navCtrl: NavController,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  login() {
    if (!this.myform.no_hp) {
      alert('Silahkan isi nomor HP.')
    } else if (!this.myform.password) {
      alert('Silahkan isi Password.')
    } else {
      this.proses_login = true;

      let url = environment.server + 'user_ub/user_ub_read_by_nohp.php';
      let body = new FormData();
      body.append('no_hp', this.myform.no_hp);

      this.http.post(url, body).subscribe((data: any) => {
        if (data) {
          var bytes = CryptoJS.AES.decrypt(data[0].password, '');
          var plaintext = bytes.toString(CryptoJS.enc.Utf8);

          if (this.myform.password == plaintext) {

            //GET FIREBASE TOKEN 
            this.firebaseX.getToken().then(token => {
              let url_token = environment.server + 'user_ub/user_ub_update_firebasetoken.php';
              let body_firebase = new FormData();
              body_firebase.append('id', data[0].id);
              body_firebase.append('firebase_token', token);

              this.http.post(url_token, body_firebase).subscribe(() => { });
            }).catch(error => console.error('Error getting token', error));

            this.navCtrl.navigateRoot('/tabs');
            this.storage.set('profil', data[0]);
          } else {
            alert('Password salah.');
          }
        } else {
          alert('No. HP belum terdaftar.');
        }

        this.proses_login = false;
      }, error => {
        console.info(error.error);
        alert('Terjadi kesalahan. ' + JSON.stringify(error.error));

        this.proses_login = false;
      });
    }
  }

}
