import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {

  constructor(
    public navCtrl: NavController,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  keluar() {
    this.storage.clear();
    this.navCtrl.navigateRoot('/login');
  }
}
