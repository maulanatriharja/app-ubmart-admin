import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss'],
})
export class StartPage implements OnInit {

  constructor(
    public navCtrl: NavController,
    public router: Router,
    public storage: Storage,
  ) { }

  ngOnInit() {
    this.storage.get('id_admin').then((val) => {
      if (val != null) {
        this.navCtrl.navigateRoot('/login');
      } else {
        // this.navCtrl.navigateRoot('/tabs/produk');
        this.router.navigate(['/app']);
      }
    });
  }

}
