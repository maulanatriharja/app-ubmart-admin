import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { environment } from '../../../environments/environment';

import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  bulan: any = moment().format('M');
  tahun: any = moment().format('YYYY');

  // total_pelanggan: number; 
  total_kurir: number;
  total_transaksi: number;
  total_laba: number;
  total_kategori: number;
  total_produk: number;

  constructor(
    public http: HttpClient,
    public platform: Platform,
    public storage: Storage,
  ) { }

  ngOnInit() {
    this.storage.get('profil').then((val) => {
      console.info(val.id);
    });
  }

  ionViewDidEnter() {
    // this.load_total_pelanggan();
    this.load_total_kurir();
    this.load_total_transaksi();
    this.load_total_laba();
    this.load_total_kategori();
    this.load_total_produk();
  }

  // load_total_pelanggan() {
  //   this.http.get(environment.server + 'total/total_user_pembeli.php').subscribe((data) => {
  //     this.total_pelanggan = data[0].total_user_pembeli;
  //   }, error => {
  //     console.info(error.error);
  //     console.info('reload pelanggan');
  //     this.load_total_pelanggan();
  //   });
  // }

  load_total_kurir() {
    this.http.get(environment.server + 'total/total_user_kurir.php').subscribe((data) => {
      this.total_kurir = data[0].total_user_kurir;
    }, error => {
      console.info(error.error);
      console.info('reload kurir');
      this.load_total_kurir();
    });
  }

  // load_total_admin() {
  //   this.http.get(environment.server + 'total/total_user_ub.php').subscribe((data) => {
  //     this.total_admin = data[0].total_user_ub;
  //   }, error => {
  //     console.info(error.error);
  //     console.info('reload admin');
  //     this.load_total_admin();
  //   });
  // }

  load_total_transaksi() {
    this.http.get(environment.server + 'total/total_transaksi.php').subscribe((data) => {
      this.total_transaksi = data[0].total_transaksi;
    }, error => {
      console.info(error.error);
      console.info('reload transaksi');
      this.load_total_transaksi();
    });
  }

  load_total_laba() {
    this.http.get(environment.server + 'total/total_laba.php?tahun=' + this.tahun + '&bulan=' + this.bulan).subscribe((data) => {
      this.total_laba = data[0].total_laba;
    }, error => {
      console.info(error.error);
      console.info('reload laba');
      this.load_total_laba();
    });
  }

  load_total_kategori() {
    this.http.get(environment.server + 'total/total_kategori.php').subscribe((data) => {
      this.total_kategori = data[0].total_kategori;
    }, error => {
      console.info(error.error);
      console.info('reload kategori');
      this.load_total_kategori();
    });
  }

  load_total_produk() {
    this.http.get(environment.server + 'total/total_produk.php').subscribe((data) => {
      this.total_produk = data[0].total_produk;
    }, error => {
      console.info(error.error);
      console.info('reload produk');
      this.load_total_produk();
    });
  }

}
