import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KurirPage } from './kurir.page';

describe('KurirPage', () => {
  let component: KurirPage;
  let fixture: ComponentFixture<KurirPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KurirPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KurirPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
