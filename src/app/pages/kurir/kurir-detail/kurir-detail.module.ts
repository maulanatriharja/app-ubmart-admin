import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KurirDetailPageRoutingModule } from './kurir-detail-routing.module';

import { KurirDetailPage } from './kurir-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KurirDetailPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [KurirDetailPage]
})
export class KurirDetailPageModule { }
