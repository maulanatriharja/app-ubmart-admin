import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KurirDetailPage } from './kurir-detail.page';

const routes: Routes = [
  {
    path: '',
    component: KurirDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KurirDetailPageRoutingModule {}
