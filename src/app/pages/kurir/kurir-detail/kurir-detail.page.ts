import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActionSheetController, AlertController, NavController, LoadingController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../../../services/events.service';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder } from "@angular/forms";
import { environment } from '../../../../environments/environment';


import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-kurir-detail',
  templateUrl: './kurir-detail.page.html',
  styleUrls: ['./kurir-detail.page.scss'],
})
export class KurirDetailPage implements OnInit {

  myForm: FormGroup;
  isSubmitted = false;
  chip_err: string;

  id_ub: number;
  id_kurir: string = '';

  constructor(
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public events: EventsService,
    public formBuilder: FormBuilder,
    public http: HttpClient,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public route: ActivatedRoute,
    public router: Router,
    public storage: Storage,
    public toastController: ToastController,
  ) {
    this.route.queryParams.subscribe(res => {

      this.id_kurir = res.id_kurir;

      this.myForm = this.formBuilder.group({
        nama_kurir: [res.nama_kurir],
        no_hp: [0 + res.no_hp],
        password: 'bismillah',
      });
    });
  }

  ngOnInit() {
    this.storage.get('profil').then((val) => {
      this.id_ub = val.ub.id_ub;
    });
  }

  async submit() {

    this.isSubmitted = true;

    if (this.myForm.valid) {

      const loading = await this.loadingController.create({});
      await loading.present();

      if (!this.id_kurir) {
        //INSERT NEW
        let url = environment.server + 'user_kurir/user_kurir_create.php';
        let body = new FormData();
        body.append('id_ub', this.id_ub.toString());
        body.append('nama_kurir', this.myForm.get('nama_kurir').value);
        body.append('no_hp', this.myForm.get('no_hp').value);
        body.append('password', CryptoJS.AES.encrypt(this.myForm.get('password').value, '').toString());

        this.http.post(url, body).subscribe(async (data) => {
          console.info(data);

          this.events.pubKurir({});
          this.navCtrl.navigateRoot('/tabs/kurir');

          loading.dismiss();

          const toast = await this.toastController.create({
            message: 'Berhasil menambah Kurir baru.',
            position: 'top',
            color: 'success',
            duration: 2000,
            buttons: [
              {
                text: 'X',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              }
            ]
          });
          toast.present();

        }, error => {
          console.info(error.error);
        });
      } else {
        //UPDATE
        let url = environment.server + 'user_kurir/user_kurir_update.php';
        let body = new FormData();
        body.append('id', this.id_kurir);
        body.append('nama_kurir', this.myForm.get('nama_kurir').value);
        body.append('no_hp', this.myForm.get('no_hp').value);

        this.http.post(url, body).subscribe(async (data) => {
          console.info(data);

          this.events.pubKurir({});
          this.navCtrl.navigateRoot('/tabs/kurir');

          loading.dismiss();

          const toast = await this.toastController.create({
            message: 'Berhasil mengubah data Kurir.',
            position: 'top',
            color: 'success',
            duration: 2000,
            buttons: [
              {
                text: 'X',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              }
            ]
          });
          toast.present();
        }, error => {
          console.info(error.error);
        });
      }
    }
  }

  async hapus() {
    const alert = await this.alertController.create({
      cssClass: 'alert-class',
      subHeader: 'Konfirmasi',
      message: 'Yakin untuk menghapus Kurir ?',
      mode: 'ios',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Hapus',
          handler: async () => {
            let url = environment.server + 'user_kurir/user_kurir_delete.php';
            let body = new FormData();
            body.append('id', this.id_kurir);

            this.http.post(url, body).subscribe(async (data) => {
              console.info(data);

              this.events.pubKurir({});
              this.navCtrl.navigateRoot('/tabs/kurir');

              const toast = await this.toastController.create({
                message: 'Kurir telah dihapus.',
                position: 'top',
                color: 'light',
                duration: 2000,
                buttons: [
                  {
                    text: 'X',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                    }
                  }
                ]
              });
              toast.present();
            }, error => {
              console.info(error.error);
            });
          }

        }
      ]
    });
    await alert.present();
  }
}
