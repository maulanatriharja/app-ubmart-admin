import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KurirDetailPage } from './kurir-detail.page';

describe('KurirDetailPage', () => {
  let component: KurirDetailPage;
  let fixture: ComponentFixture<KurirDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KurirDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KurirDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
