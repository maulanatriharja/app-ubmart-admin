import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { EventsService } from '../../services/events.service';
import { Storage } from '@ionic/storage';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-kurir',
  templateUrl: './kurir.page.html',
  styleUrls: ['./kurir.page.scss'],
})
export class KurirPage implements OnInit {

  data: any = [];
  loading = true;

  constructor(
    public events: EventsService,
    public alertController: AlertController,
    public http: HttpClient,
    public router: Router,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.events.obvKurir().subscribe(() => {
      this.load_data();
    });

    this.load_data();
  }

  load_data() {
    this.storage.get('profil').then((val) => {
      let url = environment.server + 'user_kurir/user_kurir_read.php?id_ub=' + val.ub.id_ub;

      this.http.get(url).subscribe((data) => {
        console.info(data);

        this.data = data;

        this.loading = false;
      }, error => {
        console.info(error.error);

        this.loading = false;
      });
    });
  }

  update(val_id, val_nama_kurir, val_no_hp) {
    this.router.navigate(['kurir-detail'],
      {
        queryParams: {
          id_kurir: val_id,
          nama_kurir: val_nama_kurir,
          no_hp: val_no_hp,
        }
      }
    );
  }
}
