import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KurirPageRoutingModule } from './kurir-routing.module';

import { KurirPage } from './kurir.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KurirPageRoutingModule
  ],
  declarations: [KurirPage]
})
export class KurirPageModule {}
