import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KurirPage } from './kurir.page';

const routes: Routes = [
  {
    path: '',
    component: KurirPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KurirPageRoutingModule {}
