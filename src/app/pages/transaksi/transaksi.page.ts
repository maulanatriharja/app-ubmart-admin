import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-transaksi',
  templateUrl: './transaksi.page.html',
  styleUrls: ['./transaksi.page.scss'],
})
export class TransaksiPage implements OnInit {

  sgmStatus: string = 'pending';

  data_pending: any = [];
  data_dikirim: any = [];
  data_terkirim: any = [];
  data_batal: any = [];

  loading = true;

  empty_pending = true;
  empty_dikirim = true;
  empty_terkirim = true;
  empty_batal = true;

  constructor(
    public http: HttpClient,
    public storage: Storage,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.load_data_pending();
    this.load_data_dikirim();
    this.load_data_terkirim();
    this.load_data_batal();
  }


  load_data_pending() {
    this.storage.get('profil').then((val) => {
      let url = environment.server + 'transaksi/transaksi_read.php?id_ub=' + val.ub.id_ub + '&status=pending';

      this.http.get(url).subscribe((data) => {
        if (data == null) {
          this.empty_pending = true;
        } else {
          this.data_pending = data;
          this.empty_pending = false;
        }

        this.loading = false;
      }, error => {
        console.info(error.error);
        console.info('reload_data');
        this.load_data_pending();

        this.loading = false;
      });
    });
  }

  load_data_dikirim() {
    this.storage.get('profil').then((val) => {
      let url = environment.server + 'transaksi/transaksi_read.php?id_ub=' + val.ub.id_ub + '&status=dikirim';

      this.http.get(url).subscribe((data) => {
        if (data == null) {
          this.empty_dikirim = true;
        } else {
          this.data_dikirim = data;
          this.empty_dikirim = false;
        }

        this.loading = false;
      }, error => {
        console.info(error.error);
        console.info('reload_data');
        this.load_data_dikirim();

        this.loading = false;
      });
    });
  }

  load_data_terkirim() {
    this.storage.get('profil').then((val) => {
      let url = environment.server + 'transaksi/transaksi_read.php?id_ub=' + val.ub.id_ub + '&status=terkirim';

      this.http.get(url).subscribe((data) => {
        if (data == null) {
          this.empty_terkirim = true;
        } else {
          this.data_terkirim = data;
          this.empty_terkirim = false;
        }

        this.loading = false;
      }, error => {
        console.info(error.error);
        console.info('reload_data');
        this.load_data_terkirim();

        this.loading = false;
      });
    });
  }

  load_data_batal() {
    this.storage.get('profil').then((val) => {
      let url = environment.server + 'transaksi/transaksi_read.php?id_ub=' + val.ub.id_ub + '&status=batal';

      this.http.get(url).subscribe((data) => {
        if (data == null) {
          this.empty_batal = true;
        } else {
          this.data_batal = data;
          this.empty_batal = false;
        }

        this.loading = false;
      }, error => {
        console.info(error.error);
        console.info('reload_data');
        this.load_data_batal();

        this.loading = false;
      });
    });
  }

  peta(val_lat, val_long) {
    // window.open('geo://' + val_lat + ', ' + val_long);
    window.open('geo://' + val_lat + ', ' + val_long + '?q=' + val_lat + ', ' + val_long, '_system');
  }

}