import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AdMobPro } from '@ionic-native/admob-pro/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  constructor(
    public admob: AdMobPro,
    public navCtrl: NavController,
    public platform: Platform,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar,
    public storage: Storage,
  ) {
    this.initializeApp();

    window.addEventListener('keyboardDidShow', () => {
      this.admob.hideBanner();
    });

    window.addEventListener('keyboardDidHide', () => {
      this.showAdmob();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.backgroundColorByName('black');
      this.splashScreen.hide();

      this.storage.get('profil').then((val) => {
        if (!val) {
          this.navCtrl.navigateRoot('/login');
        }
      });

      this.showAdmob();
    });
  }

  showAdmob() {
    let options = {
      adId: 'ca-app-pub-3302992149561172/2058021272',
      isTesting: false,
    }
    this.admob.createBanner(options).then(() => { this.admob.showBanner; });
  }
}
