import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

export interface Produk {
  id?: string,
  nama_produk: string,
  harga_beli: string,
  harga_jual: string,
  keterangan: string
}

@Injectable({
  providedIn: 'root'
})
export class FiredbService {

  private produk: Observable<Produk[]>;
  private produkCollection: AngularFirestoreCollection<Produk>;

  constructor(
    private afs: AngularFirestore
  ) {
    this.produkCollection = this.afs.collection<Produk>('produk');
    this.produk = this.produkCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getProduks(): Observable<Produk[]> {
    return this.produk;
  }

  getProduk(id: string): Observable<Produk> {
    return this.produkCollection.doc<Produk>(id).valueChanges().pipe(
      take(1),
      map(res => {
        res.id = id;
        return res
      })
    );
  }

  addProduk(val: Produk): Promise<DocumentReference> {
    return this.produkCollection.add(val);
  }

  updateProduk(val: Produk): Promise<void> {
    return this.produkCollection.doc(val.id).update({
      nama_produk: val.nama_produk,
      harga_jual: val.harga_jual,
      harga_beli: val.harga_beli,
      keterangan: val.keterangan
    });
  }

  deleteProduk(id: string): Promise<void> {
    return this.produkCollection.doc(id).delete();
  }
}