import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  fooSubject = new Subject<any>();

  constructor() { }

  pubKategori(data: any) { this.fooSubject.next(data); }
  obvKategori(): Subject<any> { return this.fooSubject; }

  pubProduk(data: any) { this.fooSubject.next(data); }
  obvProduk(): Subject<any> { return this.fooSubject; }

  pubAkun(data: any) { this.fooSubject.next(data); }
  obvAkun(): Subject<any> { return this.fooSubject; }

  pubKurir(data: any) { this.fooSubject.next(data); }
  obvKurir(): Subject<any> { return this.fooSubject; }

  pubUsaha(data: any) { this.fooSubject.next(data); }
  obvUsaha(): Subject<any> { return this.fooSubject; }
}
