import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages//tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'start',
    loadChildren: () => import('./pages/start/start.module').then(m => m.StartPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./pages//tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardPageModule)
  },
  {
    path: 'kategori',
    loadChildren: () => import('./pages/kategori/kategori.module').then(m => m.KategoriPageModule)
  },
  {
    path: 'produk',
    loadChildren: () => import('./pages/produk/produk.module').then(m => m.ProdukPageModule)
  },
  {
    path: 'setting',
    loadChildren: () => import('./pages/setting/setting.module').then(m => m.SettingPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'transaksi',
    loadChildren: () => import('./pages/transaksi/transaksi.module').then(m => m.TransaksiPageModule)
  },
  {
    path: 'kategori-detail',
    loadChildren: () => import('./pages/kategori/kategori-detail/kategori-detail.module').then(m => m.KategoriDetailPageModule)
  },
  {
    path: 'produk-detail',
    loadChildren: () => import('./pages/produk/produk-detail/produk-detail.module').then(m => m.ProdukDetailPageModule)
  },
  {
    path: 'transaksi-detail',
    loadChildren: () => import('./pages/transaksi/transaksi-detail/transaksi-detail.module').then(m => m.TransaksiDetailPageModule)
  },
  {
    path: 'akun',
    loadChildren: () => import('./pages/akun/akun.module').then(m => m.AkunPageModule)
  },
  {
    path: 'akun-nama',
    loadChildren: () => import('./pages/akun/akun-nama/akun-nama.module').then(m => m.AkunNamaPageModule)
  },
  {
    path: 'akun-nohp',
    loadChildren: () => import('./pages/akun/akun-nohp/akun-nohp.module').then(m => m.AkunNohpPageModule)
  },
  {
    path: 'akun-password',
    loadChildren: () => import('./pages/akun/akun-password/akun-password.module').then(m => m.AkunPasswordPageModule)
  },
  {
    path: 'kurir',
    loadChildren: () => import('./pages/kurir/kurir.module').then(m => m.KurirPageModule)
  },
  {
    path: 'kurir-detail',
    loadChildren: () => import('./pages/kurir/kurir-detail/kurir-detail.module').then(m => m.KurirDetailPageModule)
  },
  {
    path: 'usaha-jamaah',
    loadChildren: () => import('./pages/usaha-jamaah/usaha-jamaah.module').then( m => m.UsahaJamaahPageModule)
  },
  {
    path: 'usaha-jamaah-detail',
    loadChildren: () => import('./pages/usaha-jamaah/usaha-jamaah-detail/usaha-jamaah-detail.module').then( m => m.UsahaJamaahDetailPageModule)
  },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
